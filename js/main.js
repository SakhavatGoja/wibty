$('.emojis ul li a').on('click',function(e){
  e.preventDefault();
  let colorID = parseInt($(this).data('color'));
  let item = $('.playlist-item');
  $('.playlist-item').removeClass('active')
  $(`.playlist-item[data-item="${colorID}"]`).addClass('active')
})


$(window).scroll(function() {

  var inColorMenu = false;
  var top_of_screen = $(window).scrollTop();

  $('.section_white').each(function(i) {
    var top_of_element = $(this).offset().top;
    var bottom_of_element = top_of_element + $(this).outerHeight();

    if ((top_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
      inColorMenu = true;
      return false;
    }
  });

  if (inColorMenu) {
    $("section.header").addClass("change");
  } else {
    $("section.header").removeClass("change");
  }
});

$('.menu-icon').click(function(e){
  e.preventDefault()
  $('.sidebar').css('right','0')
})

$('.closebtn').click(function(e){
  e.preventDefault()
  $('.sidebar').css('right','-1000px')
})